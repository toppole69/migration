#!/usr/bin/env node

"use strict";

const fs = require("fs");

function readJSON(path) {
  return new Promise((resolve, reject) => {
    fs.readFile(path, "utf8", function(err, data) {
      if (err) {
        return reject(err);
      }
      try {
        resolve(JSON.parse(data));
      } catch (e) {
        reject(e);
      }
    });
  });
}

function isSubstitutableDatasourceName(datasourceName) {
  if (!datasourceName) return false;
  if (datasourceName === "-- Grafana --") return false;
  if (datasourceName.startsWith("$")) return false;
  return true;
}

function collectDatasources(json) {
  if (!json) return null;
  if (typeof json !== "object") return null;

  let set = Object.keys(json).reduce((memo, key) => {
    let v = json[key];
    if (key === "datasource" && typeof v === "string") {
      if (isSubstitutableDatasourceName(v)) {
        memo[v] = 1;
      }
    } else {
      let subDatasources = collectDatasources(v);
      if (subDatasources) {
        subDatasources.forEach(ds => (memo[ds] = 1));
      }
    }

    return memo;
  }, {});

  return Object.keys(set);
}

function dataSourceToInput(datasourceName) {
  if (!isSubstitutableDatasourceName(datasourceName)) return datasourceName;

  return "DS_" + datasourceName.toUpperCase().replace(/ /g, "_");
}

function dataSourceToVariable(datasourceName) {
  if (!isSubstitutableDatasourceName(datasourceName)) return datasourceName;

  return `\$\{${dataSourceToInput(datasourceName)}\}`;
}

function fixPrometheusEnvironmentSelector(promql) {
  return promql.replace(/(environment)(\s*)(=|!=|=~)(\s*)"(prd|stg|cny)"/g, function replacer(match, label, s1, comparator, s2, value) {
    return `${label}${s1}${comparator}${s2}"g${value}"`;
  });
}

// This function assumes, since the source data is JSON, that there are no
// circular references in the data
function substituteDataSources(json) {
  if (!json || typeof json !== "object") return json;

  Object.keys(json).forEach(key => {
    let v = json[key];
    if (!v) return;

    if (key === "datasource" && typeof v === "string") {
      json[key] = dataSourceToVariable(v);
    } else if ((key === "query" || key === "expr") && typeof v === "string") {
      json[key] = fixPrometheusEnvironmentSelector(v);
    } else {
      json[key] = substituteDataSources(v);
    }
  });

  return json;
}

async function transform(path) {
  let json = await readJSON(path);
  let datasources = collectDatasources(json);

  let inputs = datasources.map(ds => {
    return {
      name: dataSourceToInput(ds),
      label: ds,
      description: "",
      type: "datasource",
      pluginId: "prometheus",
      pluginName: "Prometheus"
    };
  });

  let requires = [
    {
      type: "grafana",
      id: "grafana",
      name: "Grafana",
      version: "4.6.3"
    },
    {
      type: "panel",
      id: "graph",
      name: "Graph",
      version: ""
    },
    {
      type: "datasource",
      id: "prometheus",
      name: "Prometheus",
      version: "1.0.0"
    }
  ];

  let dashboard = substituteDataSources(json.dashboard);

  let result = { __inputs: inputs, __requires: requires, ...dashboard };

  return result;
}

function run(path) {
  return transform(path)
    .then(function(result) {
      console.log(JSON.stringify(result, null, "  "));
    })
    .then(function() {
      process.exit(0);
    })
    .catch(function(err) {
      console.error(err);
      process.exit(1);
    });
}

run(process.argv[2]);
